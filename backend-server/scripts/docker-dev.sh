set -e

./scripts/docker-build.sh dev

docker stop job-server || true
docker rm job-server || true

docker run --name job-server -p 8100:8100 job-server:dev
