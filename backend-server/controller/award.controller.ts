import { RestfulController } from "./rest.controller";
import {AwardService, AwardTypeService} from "../service/award.service"
import express from "express";
import { requireJWTPayload } from "../jwt-helper";
import { HttpError } from "../http-error";

export class AwardController extends RestfulController {
    constructor(private awardService: AwardService,
        private awardTypeService: AwardTypeService){
        super();
        this.router.get("/award/list",this.handleRequest(this.getAwardList));
        this.router.get("/award/unPayment",this.handleRequest(this.unPaymentList));
        this.router.post("/award/create",this.handleRequest(this.createAward));
        this.router.get("/award/type/list",this.handleRequest(this.getAwardType));
        this.router.post("/award/type/create",this.handleRequest(this.createAwardType));
    }
    is_admin = async  (req: express.Request) => {
        let jwt = await requireJWTPayload(req);
        let is_admin = jwt.payload.is_admin;
        if(!is_admin){
            throw new HttpError("admin only", 400);
        }
        let id = jwt.payload.id
        return id 
    }
    getAwardList= async(req:express.Request)=>{
        await this.is_admin(req)
        return this.awardService.getAwardList()
    }


    unPaymentList = async(req:express.Request)=>{
        await this.is_admin(req)
        return this.awardService.unPaymentList()
    }

    createAward = async(req:express.Request)=>{
        await this.is_admin(req)
        let {employee_id, award_type_id , amount,desc} = req.body
        return await this.awardService.createAward(employee_id, award_type_id , amount,desc)
    }

    getAwardType= async(req:express.Request)=>{
        await this.is_admin(req)
        return await this.awardTypeService.getAwardTypeList()
    }

    createAwardType = async(req:express.Request)=>{
        await this.is_admin(req)
        let {name} = req.body
        return await this.awardTypeService.createAwardType(name)
    }

}