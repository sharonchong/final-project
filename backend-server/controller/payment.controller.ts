import { RestfulController } from "./rest.controller";
import {PaymentService} from "../service/payment.service"
import express from "express";
import { requireJWTPayload } from "../jwt-helper";
import { HttpError } from "../http-error";
import { AwardService } from "../service/award.service";
import { JobRoleService } from "../service/jobRole.service";


export class PaymentController extends RestfulController {
    constructor(private paymentService: PaymentService,
        private awardService : AwardService,
        private jobRoleService : JobRoleService){
        super();
        this.router.post("/payment/award",this.handleRequest(this.createPaymentWithAward));
        this.router.post("/payment/job",this.handleRequest(this.createPaymentWithJob));
        this.router.post("/payment/read",this.handleRequest(this.readPayment));
    }

    is_admin = async  (req: express.Request) => {
        let jwt = await requireJWTPayload(req);
        let is_admin = jwt.payload.is_admin;
        if(!is_admin){
            throw new HttpError("admin only", 400);
        }
        let id = jwt.payload.id
        return id 
    }
    user_id = async  (req: express.Request) => {
        let jwt = await requireJWTPayload(req);
        let id = jwt.payload.id;
        if(!id){
            throw new HttpError("user not found", 400);
        }
        return id 
    }

    createPaymentWithAward = async(req:express.Request)=>{
        let admin_id = await this.is_admin(req)
        let {id} = req.body
        let payment_id = await this.paymentService.createPayment(admin_id)
        let result = await this.awardService.paymentAward(id,payment_id)
        return result
    }
    createPaymentWithJob = async(req:express.Request)=>{
        let admin_id = await this.user_id(req)
        let id = req.body.id
        let payment_id = await this.paymentService.createPayment(admin_id)
        let result = await this.jobRoleService.paymentJobRole(id,payment_id)
        return result
    }

    readPayment= async(req:express.Request)=>{
        let {id} = req.body
        return this.paymentService.readPayment(id)
    }


}