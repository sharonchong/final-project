import express from "express";
import { HttpError } from "../http-error";
import { EmployeeService } from "../service/employee.service";
import { RestfulController } from "./rest.controller";
import { Multer } from 'multer'
import { UserService } from "../service/user.service";
import { EmployeeDistrictService } from "../service/district.service";
import { requireJWTPayload } from "../jwt-helper";
import { Employee } from "../models";
import { s3 } from "../upload";
import { env } from "../env";

export class EmployeeController extends RestfulController {
  constructor(private employeeService: EmployeeService,
              private upload: Multer,
              private userService: UserService,
              private employeeDistrictService: EmployeeDistrictService) {
    super();
    this.router.post("/employee/profile/:user_id", this.upload.fields([{name: 'icon_file' , maxCount :1},{name:'attach_file', maxCount : 1}]), this.handleRequest(this.createEmployee))
    this.router.get("/employee/profile/:user_id", this.handleRequest(this.getEmployeeProfile))
    this.router.get("/employee/JobRequest/:user_id", this.handleRequest(this.getPendingJobList))
  }
  
  createEmployee = async (req: express.Request, res: express.Response) => {
    let user_id = +req.params.user_id
    let jwt = await requireJWTPayload(req);
    let { bank_account, fullname, nickname, gender, salary} = req.body;
    if(!fullname || !nickname || !gender){
      throw new HttpError("Please Enter info", 400)
    }
    let district_list : number[] = req.body.district_list?.split(',')
    if(user_id != jwt.payload.id){
      if(!jwt.payload.is_admin){
        throw new HttpError("Admin Only", 401)
      }
    }
    let icon
    let attach
    if(!Array.isArray(req.files) && req.files){
      icon = req.files['icon_file']?.[0]?.key  || req.files['icon_file']?.[0]?.filename
      attach = req.files['attach_file']?.[0]?.key  || req.files['attach_file']?.[0]?.filename
    }
    console.log(icon,attach)
    if(icon){
    let row = await this.userService.getIconLink(user_id)
      if (row?.icon) {
        s3.deleteObject({Key: row.icon, Bucket: env.aws.s3Bucket}, (error) => {
          if (error) {
            console.log("Failed to cleanup s3 attach:", error)
          }
        })
      }
    }
    if(attach){
    let row = await this.employeeService.getAttachLink(user_id)
      if (row?.attach) {
        s3.deleteObject({Key: row.attach, Bucket: env.aws.s3Bucket}, (error) => {
          if (error) {
            console.log("Failed to cleanup s3 attach:", error)
          }
        })
      }
    }
    if (nickname || icon){
      let row = {nickname, icon}
      await this.userService.updateUser(user_id, row)
    }
    if (district_list){  
      await this.employeeDistrictService.createEmployeeDistrict(user_id, district_list)
    } else {
      throw new HttpError("missing district", 400)
    }
    let employee: Employee = { gender , salary , bank_account, fullname, attach}
    let result = await this.employeeService.createEmployee( user_id , employee)
    await this.userService.updateUserState(user_id, true)  //
    let newtoken = await this.userService.createToken(jwt.payload.id)
    let token = newtoken.token
    return {token, ...result}
  }

  getEmployeeProfile = async (req: express.Request, res: express.Response) => {
    let user_id = +req.params.user_id
    let jwt = await requireJWTPayload(req);
    if(user_id != jwt.payload.id){
      if(!jwt.payload.is_admin){
        throw new HttpError("Admin Only", 401)
      }
    }
    let profile = await this.employeeService.getEmployeeProfile(user_id)
    let district_list = await this.employeeDistrictService.getEmployeeDistrict(user_id)
    // let data = {
    //   nickname: "",
    //   gender: "",
    //   salary: "",
    //   district_list,
    //   bank_account: "",
    //   fullname: "",
    //   icon: "",
    //   attach: "",
    //   ...profile
    // }
    return {...profile , district_list}
  }
  


getPendingJobList= async (req:express.Request, res:express.Response)=>{
  let user_id = +req.params.user_id
  console.log(user_id)

  try{
    let list = await this.employeeService.getPendingJobList(user_id);
   return({list});
  
  }catch (error) {
    res.status(500).json({error:String(error)})
  }
}

}

