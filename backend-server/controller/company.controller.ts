import express from "express";
import { HttpError } from "../http-error";
import { requireAdminJWT, requireJWTPayload } from "../jwt-helper";
import { Company } from "../models";
import { CompanyRoleService, CompanyService } from "../service/company.service";
import { UserService } from "../service/user.service";
import { RestfulController } from "./rest.controller";
import { Multer } from "multer";
import { s3 } from "../upload";
import { env } from "../env";

export class CompanyController extends RestfulController {
  constructor(
    private companyService: CompanyService,
    private companyRoleService: CompanyRoleService,
    private userService: UserService,
    private upload: Multer
  ) {
    super();
    this.router.get(
      "/company/profile",
      this.handleRequest(this.getCompanyProfile)
    );
    this.router.post(
      "/company",
      this.upload.single("icon_file"),
      this.handleRequest(this.createCompany)
    );
    this.router.get(
      "/company/companyRoleList",
      this.handleRequest(this.getCompanyRoleList)
    );
    this.router.post(
      "/company/banCompanyRole",
      this.handleRequest(this.banCompanyRole)
    );
    this.router.post(
      "/company/role/verify",
      this.handleRequest(this.verifyCompanyRole)
    );
    this.router.post("/company/verify", this.handleRequest(this.verifyCompany));
  }

  user_id = async (req: express.Request) => {
    let jwt = await requireJWTPayload(req);
    let id = jwt.payload.id;
    if(!id){
        throw new HttpError("user not found", 400);
    }
    return id
 
  };
  company_id = async (req: express.Request) => {
    let jwt = await requireJWTPayload(req);
    let company_id = jwt.payload.company_id;
    if(!company_id){
        throw new HttpError("user not found", 400);
    }
    return company_id

  };

  getCompanyProfile = async (req: express.Request) => {
    let jwt = await requireJWTPayload(req);
    let company_id = jwt.payload.company_id;
    return this.companyService.getCompanyProfile(company_id );
  };

  createCompany = async (req: express.Request) => {
    let jwt = await requireJWTPayload(req);
    let user_id = jwt.payload.id;
    let { nickname } = req.body;
    let { name, telephone, address, email, bank_account, principal } = req.body;
    let icon = req.file?.key || req.file?.filename;
    if (icon) {
      let row = await this.userService.getIconLink(user_id);
      if (row?.icon) {
        s3.deleteObject(
          { Key: row.icon, Bucket: env.aws.s3Bucket },
          (error) => {
            if (error) {
              console.log("Failed to cleanup s3 attach:", error);
            }
          }
        );
      }
    }

    if (!nickname) {
      throw new HttpError("missing nickname in req.body", 400);
    }
    if (!name) {
      throw new HttpError("missing company name in req.body", 400);
    }
    if (!telephone) {
      throw new HttpError("missing telephone  in req.body", 400);
    }
    if (!address) {
      throw new HttpError("missing address in req.body", 400);
    }
    if (!email) {
      throw new HttpError("missing email in req.body", 400);
    }
    if (!bank_account) {
      throw new HttpError("missing bank_account in req.body", 400);
    }
    if (!principal) {
      throw new HttpError("missing principal in req.body", 400);
    }
    if (nickname || icon) {
      let row = { nickname, icon };
      await this.userService.updateUser(user_id, row);
    }
    let row = await this.companyService.getCompanyIdByName(name);
    if (row) {
      throw new HttpError("Company has been created", 400);
    }
    let company: Company = {
      name,
      telephone,
      address,
      email,
      bank_account,
      principal,
    };
    let company_id = await this.companyService.createCompany(company, user_id);
    await this.companyRoleService.createCompanyRole(user_id, company_id);
    await this.userService.updateUserState(user_id, true); //
    let newtoken = await this.userService.createToken(jwt.payload.id)
    let token = newtoken.token
    return {
      message:
        "waiting admin approval, your company name" + name + ", Company ID: ",
       token
    };
  };
  banCompanyRole = async (req: express.Request) => {
    let { user_id } = req.body;
    let company_id = await this.company_id(req);
    let admin_id = await this.user_id(req);
    if (user_id == admin_id) {
      throw new HttpError("Can not ban", 400);
    }
    await this.companyRoleService.banCompanyRole(user_id, company_id, admin_id);
  };
  getCompanyRoleList = async (req: express.Request) => {
    let company_id = await this.company_id(req);
    return await this.companyRoleService.getCompanyRoleListByCompany(
      company_id
    );
  };

  verifyCompanyRole = async (req: express.Request) => {
    let { user_id } = req.body;
    let company_id = await this.company_id(req);
    let admin_id = await this.user_id(req);
    await this.userService.updateUserState(user_id, true);
    await this.companyRoleService.verifyCompanyRole(
      user_id,
      company_id,
      admin_id
    );
  };
  verifyCompany = async (req: express.Request) => {
    let jwt = await requireAdminJWT(req);
    let admin_id = jwt.payload.id;
    let { company_id, user_id } = req.body;
    if (!company_id) {
      throw new HttpError("missing company_id in req.body", 400);
    }
    if (!user_id) {
      throw new HttpError("missing uder_id in req.body", 400);
    }
    await this.companyService.approveCompany({ company_id, admin_id, user_id });
    return { token: jwt.token };
  };
}
