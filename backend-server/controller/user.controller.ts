import express from "express";
import { HttpError } from "../http-error";
import { UserService } from "../service/user.service";
import { RestfulController } from "./rest.controller";
import { requireJWTPayload } from "../jwt-helper";

export class UserController extends RestfulController {
  constructor(private userService: UserService) {
    super();
    this.router.get("/user/profile", this.handleRequest(this.getUserProfile));
    this.router.post("/login/email",this.handleRequest(this.requestPasscodeWithEmail));
    this.router.post("/login/tel",this.handleRequest(this.requestPasscodeWithTel));
    this.router.post("/login/passcode", this.handleRequest(this.requestToken));
    this.router.post("/update/user", this.handleRequest(this.updateUser));
    this.router.get("/update/token", this.handleRequest(this.updateToken));
  }

  user_id = async  (req: express.Request) => {
    let jwt = await requireJWTPayload(req);
    let id = jwt.payload.id;
    if(!id){
        throw new HttpError("user not found", 400);
    }
    return id 
}

  requestPasscodeWithEmail = async (
    req: express.Request,
    res: express.Response
  ) => {
    let { email } = req.body;
    if (!email) {
      throw new HttpError("missing email in req.body",404);
    }
    await this.userService.sendPasscodeByEmail(email);
    return { message: "sent to your email" };
  };

  requestPasscodeWithTel = async (
    req: express.Request,
    res: express.Response
  ) => {
    let { tel } = req.body;
    if (!tel) {
      throw new HttpError("missing tel in req.body",400);
    }
    if( tel.length != 8){
      throw new HttpError("tel format error",400)
    }
    tel = '+852' + tel
    await this.userService.sendPasscodeByTel(tel);
    return { message: "sent to your mailbox" };
  };

  requestToken = async (req: express.Request, res: express.Response) => {
    let { email, tel, passcode, invite_code } = req.body;
    let contact
    if(email){contact  = email}
    else{ contact = tel}
    if (!contact) {
      throw new HttpError("missing email or tel in req.body",400);
    }
    if (!passcode) {
      throw new HttpError("missing passcode in req.body",400);
    }
    let token = await this.userService.getToken(contact, passcode);
    if (invite_code){
      let id = token.payload.id
      await this.userService.setReferrer({id,invite_code})
    }
    return { token: token.token };;
  };

  getUserProfile = async (req: express.Request, res: express.Response) => {
    let id = await this.user_id(req)
    return this.userService.getUser(id);
  };

  updateToken  = async (req: express.Request, res: express.Response) => {
    let id = await this.user_id(req)
    let token = await this.userService.createToken(id)
    return { token: token.token };
  }

  updateUser = async (req: express.Request, res: express.Response) => {
    let { icon, nickname, mobile, email } = req.body;
    let id = await this.user_id(req)
    //console.log(req)
    let fields = ["nickname"];
    for (let field of fields) {
      if (!req.body[field]) {
        throw new HttpError("missing " + field, 400);
      }
    }
    let row = { icon, nickname,mobile, email };
    await this.userService.updateUser(id, row);
    let token = await this.userService.createToken(id)
    return { message: "updated", token: token.token };
  };


}
