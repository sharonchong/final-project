import { RestfulController } from "./rest.controller";
import { MessageService } from "../service/message.service";
import express from "express";
import { requireJWTPayload } from "../jwt-helper";
import { HttpError } from "../http-error";
import { UserService } from "../service/user.service";
import { JobService } from "../service/job.service";
import { io } from "../socket-io"

export class MessageController extends RestfulController {
  constructor(
    private messageService: MessageService,
    private userService: UserService,
    private jobService: JobService
  ) {
    super();
    this.router.get("/message/:type/:id", this.handleRequest(this.getMessage));
    this.router.get("/message/info/:type/:id", this.handleRequest(this.getMessageInfo));
    this.router.post(
      "/message/:type/:id",
      this.handleRequest(this.sendMessage)
    );
    this.router.get("/message/list", this.handleRequest(this.getMessageList));
  }

  user_id = async (req: express.Request) => {
    let jwt = await requireJWTPayload(req);
    let id = jwt.payload.id;
    if (!id) {
      throw new HttpError("user not found", 400);
    }
    return id;
  };

  sendMessage = async (req: express.Request) => {
    let type = req.params.type;
    let roomNumber = +req.params.id;
    let sender_id = await this.user_id(req);
    let { content } = req.body;
    let time = new Date()
    if (type == "job") {
      let jwt = await requireJWTPayload(req);
      let nickname = jwt.payload.nickname
      let row = { sender_id, job_id: roomNumber, content };
      let result = await this.messageService.sendMessage(row);
      io.to(`${type}-${roomNumber}`).emit('newMessage', {data:{"id": result,"user_id":sender_id,"content" :content, "send_time": time, "job_id": roomNumber ,"nickname":nickname}})
    return result
    } else if (type == "user") {
      let row = { sender_id, receiver_id: roomNumber, content };
      let result = await this.messageService.sendMessage(row);
      io.to(`${type}-${roomNumber}`).emit('newMessage', {data:{"id": result,"user_id":sender_id,"content" :content,"send_time": time}})
      return result
    }
    return "error";
  };
  getMessageInfo  = async (req: express.Request) => {
    let type = req.params.type;
    let roomNumber = +req.params.id;
    // console.log(req.params)
    if (type == "job") {
      let room = await this.jobService.getRoomName(roomNumber);
      return {room}
  }else if (type == "user") {
    let room = await this.userService.getRoomName(roomNumber);
    let online = await this.userService.getUserOnlineStatus(roomNumber)
    return {room, online}
  }
  return 'error'
}

  getMessage = async (req: express.Request) => {
    let type = req.params.type;
    let roomNumber = +req.params.id;
    let item
    if(req.query.item){
      item = +req.query.item
    } else { item = 20}
    let id = await this.user_id(req);
    if (type == "job") {
      let data = await this.messageService.getJobMessage(id, roomNumber, item);
      data = data.reverse()
      return { data };
    } else if (type == "user") {
      let data = await this.messageService.getUserMessage(id, roomNumber, item);
      data = data.reverse()
      return { data};
    }
    return "error";
  };

  getMessageList = async (req: express.Request) => {
    let id = await this.user_id(req);
    let data = await this.messageService.getMessageList(id);
    return { data };
  };
}
