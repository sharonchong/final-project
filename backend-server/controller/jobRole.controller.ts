import { RestfulController } from "./rest.controller";
import express from "express";
import { HttpError } from "../http-error";
import { requireJWTPayload } from "../jwt-helper";
import { JobRoleService } from "../service/jobRole.service";

export class JobRoleController extends RestfulController {
    constructor(private jobRoleService: JobRoleService) {
        super();
        this.router.post("/job/role/create", this.handleRequest(this.createJobRole))
        this.router.post("/job/role/confirm", this.handleRequest(this.confirmJobRoleByEmployee))
        this.router.post("/job/role/reject", this.handleRequest(this.rejectJobRoleByEmployee))
        this.router.post("/job/role/score", this.handleRequest(this.scoreJobRoleByCompany))
        this.router.post("/job/company/specialAward", this.handleRequest(this. specialAwardByCompany ))
       
        this.router.get("/job/:job_id/employee-list", this.handleRequest(this.getJobRoleEmployee))
    }

    employee_id = async (req: express.Request) => {
        let jwt = await requireJWTPayload(req);
        let id = jwt.payload.id;
        if (!id) {
            throw new HttpError("user not found", 400);
        }
        return id
    }

    createJobRole = async (req: express.Request) => {
        let employee_id = await this.employee_id(req)
        let { job_id} = req.body
        let id = await this.jobRoleService.createJobRole(job_id, employee_id)
        return id
    }

    confirmJobRoleByEmployee = async (req: express.Request) => {
        let employee_id = await this.employee_id(req)
        let { job_id } = req.body
        console.log(job_id,employee_id)
        let confirm = await this.jobRoleService.confirmJobRole(job_id, employee_id)
        await this.jobRoleService.createJobRole(job_id, employee_id)
        return confirm
    }

    rejectJobRoleByEmployee = async (req: express.Request) => {
        let employee_id = await this.employee_id(req)
        let { job_id } = req.body
        let confirm = await this.jobRoleService.rejectJobRole(job_id, employee_id)
        await this.jobRoleService.createJobRole(job_id, employee_id)
        return confirm
    }
    
    rejectJobRoleByCompany = async (req: express.Request) => {
        let { id } = req.body
        console.log(id)
        return await this.jobRoleService.rejectJobRoleFromCompany(id)
    }
    specialAwardByCompany = async (req: express.Request) => {
        let { id,specialAward } = req.body
        console.log(id)
        return await this.jobRoleService.specialAwardFromCompany(id,specialAward)
    }

    scoreJobRoleByCompany = async (req: express.Request) => {
        let { id, score } = req.body
        if (score > 5) {
            throw new HttpError("score more than 5", 400);
        }
        return await this.jobRoleService.scoreJobRole(id, score)
    }
    getJobRoleEmployee = async (req: express.Request, res: express.Response) => {
        let { job_id } = req.params
        
        if (!job_id) {
            throw new HttpError("Missing job_id", 400);
        }
        let list = await this.jobRoleService.getJobEmployeeList(+job_id);
        console.log(list)
        return { list }

    }
}