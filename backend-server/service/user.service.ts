import { Knex } from "knex";
import { HttpError } from "../http-error";
import { JWTPayload } from "../models";
import { DAY } from "./time";
import jwt from "jwt-simple";
import { jwtConfig } from "../jwt";
import { EmailService } from "./email.service";
import { SmsService } from "./sms.service";
import { env } from "../env";

const PASSCODE_EXPIRE_INTERVAL = 1000 * 60 * 5; // 5 minutes
const GOOGLE_EMPLOYEE = "google_employee@example.net"
const GOOGLE_COMPANY = "google_company@example.net"
const GOOGLE_PASSCODE = "GOOGLE"

export class UserService {
  private passcode_map = new Map<string, { contact: string; timer: any }>();
  private contact_trial_map = new Map<string, number>();
  private onlineUsers = new Set<number>();
  constructor(
    private knex: Knex,
    public emailService: EmailService,
    public smsService: SmsService
  ) {}

  getUserOnlineStatus(id : number){
    // console.log(this.onlineUsers)
    // console.log({id})
    return {
      status : this.onlineUsers.has(id) ? 'online' : 'offline'
    }
  }

  markUserOnline(id: number){
    this.onlineUsers.add(id)
    // console.log(this.onlineUsers)
    console.log('Mark Online count:', this.onlineUsers.size)
  }
  unUserOnline(id: number){
    this.onlineUsers.delete(id)
    // console.log(this.onlineUsers)
    console.log('UnMark Online count:', this.onlineUsers.size)
  }

  table() {
    return this.knex("users");
  }
  
  async createToken(user_id: number) {
    let payload: JWTPayload = await this.getJwtPayload(user_id)
    let token = jwt.encode(payload, jwtConfig.jwtSecret);
    return { token, payload };
  }

  async decodeToken(token: string) {
    let payload: JWTPayload;
    try {
      payload = jwt.decode(token, jwtConfig.jwtSecret);
    } catch (error) {
      throw new HttpError("Invalid JWT token: " + error, 401);
    }
    if (payload.is_admin) {
      if (payload.exp <= Date.now()) {
        let id = payload.id;
        return await this.createToken(id);
      }
    }
    return { payload, token:undefined };
  }

  async getJwtPayload(id: number) {
    let result = await this.table()
      .select(
        "id","nickname","enable",
        this.knex.raw(
          "(select true from admin where user_id = users.id and delete_time is null) as is_admin"
        ),
        this.knex.raw(
          "(select true from employee where user_id = users.id) as is_employee"
        ),
        this.knex.raw(
          "(select company_id from company_role where ban_role_time is null and user_id = users.id limit 1) as company_id"
        )
      )
      .where({ id })
      .first();
      if(!result){
        throw new HttpError("user not found",404)
      }
      if (result.is_admin){
        result.exp = Date.now() + 90 * DAY
      }
    return result;
  }

  //   select
  //   user.id
  // , (select true from admin where user_id = user.id and delete_time is null) as is_admin
  // , (select company_id from company_role where ban_role_time is null and user_id = user.id limit 1) as company_id
  // from user

  async setReferrer(user: { id: number; invite_code: number }) {
    let row = await this.table()
      .select("id")
      .where({ invite_code: user.invite_code })
      .first();
    if (!row) {
      throw new HttpError("Incorrect Invitation code", 400);
    }
    await this.table()
      .update({ referrer_id: row.id })
      .whereNull("referrer_id")
      .andWhere({ id: user.id });
  }

  async getUserIdByContact(contact: string) {
    let user = contact.includes("@") ? { email: contact } : { tel: contact };
    let row = await this.table().select("id").where(user).first();
    if (row) {
      return row.id as number;
    }
    for (;;) {
      let invite_code = Math.random().toString().slice(-6);
      let row = await this.table().select("id").where({ invite_code }).first();
      if (row) {
        continue;
      }
      let rows: any[] = await this.table()
        .insert({ ...user, invite_code })
        .returning("id");
      let id = rows[0].id as number;
      return id;
    }
  }

  async sendPasscodeByEmail(email: string) {
    if(email === GOOGLE_COMPANY || email === GOOGLE_EMPLOYEE){
      return email
    }
    await this.sendPasscode(async (passcode) => {
      let href = `${env.REACT_APP_ORIGIN}/login/passcode/${passcode}`;
      await this.emailService.sendEmail({
        email_address: email,
        title: `Login to ${env.APP_NAME}`,
        body: /* html */ `
        <h1>Welcome to ${env.APP_NAME}</h1>
        <p>You can use the following link to login</p>
        <a href="${href}">${href}</a>`,
      });
      return email;
    });
  }

  async sendPasscodeByTel(tel: string) {
    await this.sendPasscode(async (passcode) => {
      await this.smsService.sendMessage({
        tel,
        content: `${passcode} is your verification code for ${env.APP_NAME}`,
      });
      return tel;
    });
  }

  async sendPasscode(sendPasscodeFn: (passcode: string) => Promise<string>) {
    for (;;) {
      let passcode = Math.random().toString().slice(-6);
      if (this.passcode_map.has(passcode)) {
        continue;
      }
      console.log(passcode)
      let contact = await sendPasscodeFn(passcode);

      let timer = setTimeout(() => {
        this.passcode_map.delete(passcode);
      }, PASSCODE_EXPIRE_INTERVAL);

      this.passcode_map.set(passcode, { contact: contact, timer });
      this.contact_trial_map.set(contact, 0);

      return;
    }
  }

  async getToken(contact: string, passcode: string) {
    if(contact == GOOGLE_COMPANY || contact == GOOGLE_EMPLOYEE){
      if(passcode != GOOGLE_PASSCODE){
        throw new HttpError("wrong passcode",400);
      }
    } else {
    let trial = this.contact_trial_map.get(contact);
    if (trial === undefined) {
      throw new HttpError("wrong email or tel",400);
    }
    if (trial > 3) {
      throw new Error("passcode expired, please retry later");
    }
    this.contact_trial_map.set(contact, trial + 1);
    let pair = this.passcode_map.get(passcode);
    if (!pair) {
      throw new Error("wrong or expired passcode");
    }
    if (contact !== pair.contact) {
      throw new Error("invalid passcode");
    }
    this.passcode_map.delete(passcode);
    clearTimeout(pair.timer);
  }
    let id = await this.getUserIdByContact(contact);
    let token = await this.createToken(id);
    return token;
  }

  async getUser(id: number) {
    let member_id = id;
    if (!member_id) {
      throw new HttpError("Error id", 400);
    }
    let row = await this.table()
      .select("id", "nickname", "email", "icon", "mobile")
      .where({ id })
      .first();
    if (!row) {
      throw new HttpError("user row not found", 404);
    }
    return row;
  }

  async getUserList() {
    let row: any[] = await this.table().select(
      "id",
      "nickname",
      "email",
      "mobile",
      "icon",
      "enable",
      "referrer_id",
      "invite_code",
    );
    if (row.length === 0) {
      throw new HttpError("user row not found", 404);
    }
    return row;
  }
  async getWaitingApprove() {
    let row: any[] = await this.table().select(
      "id",
      "nickname",
      "email",
      "mobile",
      "icon",
      "referrer_id",
      "invite_code",
      "enable",
    ).where("enable","false");
    if (row.length === 0) {
      throw new HttpError("user row not found", 404);
    }
    return row;
  }


  async employeeApproval() {
    let row: any[] = await this.table().select(
      "id",
      "nickname",
      "email",
      "mobile",
      "icon",
      "enable",
      "referrer_id"
    ).where("enable",false);
    if (row.length === 0) {
      throw new HttpError("user row not found", 404);
    }
    return row;
  }

  async updateUser(id: number,row: {nickname?: string; icon?: string }
  ) {
    let result = await this.table().update(row).where({ id });
    if (!result) {
      throw new HttpError("user row not found", 404);
    }
    return true;
  }

  async userApprove(id: number) {
    let enable = true
    let result = await this.table().update({ enable }).where({ id })
    if (!result) {
        throw new HttpError("Approval Fail", 400)
    }
    return { message: "Approved" }
}


  async updateUserState(id: number, enable : boolean){
    let updated_at = new Date()
    await this.table().update({enable, updated_at}).where({id})
    return {message: "update user " + id +"enable: " + enable} 
  }

  async getIconLink ( id : number){
    return await this.table().select("icon").whereNotNull("icon").andWhere({id}).first()
  }

  async getRoomName(id: number) {
    let member_id = id;
    if (!member_id) {
      throw new HttpError("Error id", 400);
    }
    let row = await this.table()
      .select("icon", "nickname as roomName")
      .where({ id })
      .first();
    if (!row) {
      throw new HttpError("user row not found", 404);
    }
    return row;
  }

  async updateUserAccountAttribute(val:string | number, userId: number, attribute: any) {
    const obj:any = {}
    obj[attribute] = val;

    await this.table().update(obj).where("id",userId);
    return true
  }

}
