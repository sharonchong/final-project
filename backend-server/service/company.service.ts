import { Knex } from "knex";
import { HttpError } from "../http-error";
import { Company } from "../models";


export class CompanyService {
  constructor(private knex: Knex) { }

  table() {
    return this.knex("company");
  }

  async getCompanyProfile(id: number) {
    let row = await this.table()
      .select("id", "name", "telephone", "address", "email", "bank_account", "principal")
      .where({ id }).first();
    if (!row) {
      throw new HttpError("company row not found", 404);
    }
    return row;
  }

  async createCompany(company: Company, user_id: number) {
    let result = await this.table().insert(company).returning("id");
    let company_id = result[0].id as number;
    await this.knex('company_role').insert({ user_id, company_id })

    return company_id
  }

  async getCompanyIdByName(name: string) {
    let row = await this.table().select("id").where({ name }).first();
    if (!row) {
      return null;
    }
    return row.id as number;
  }

  async getCompanyList() {
    return await this.table().select()
  }

  async approveCompany(input: { company_id: number, admin_id: number, user_id: number }) {
    await this.knex.transaction(async (knex) => {
      await knex('company_role').update({ admin_id: input.admin_id }).where({ user_id: input.user_id, company_id: input.company_id })
      let updated_at = new Date()
      await knex('users').update({ enable: true, updated_at }).where({ id: input.user_id })
    })

  }

  async updateCompanyInfoAttribute(val:string | number, companyId: number, attribute: any) {
    const obj:any = {}
    obj[attribute] = val;

    await this.table().update(obj).where("id",companyId);
    return true
  }

}

export class CompanyRoleService {
  constructor(private knex: Knex) { }

  table() {
    return this.knex("company_role");
  }

  async createCompanyRole(user_id: number, company_id: number) {
    let row = await this.table().select("user_id").where({ user_id, company_id }).first();
    if (!row) {
      await this.table().insert({ user_id, company_id })
      return true
    }
    return true
  }

  async verifyCompanyRole(user_id: number, company_id: number, admin_id: number) {
    await this.table().update({ admin_id }).where({ user_id, company_id })
    return true
  }

  async banCompanyRole(user_id: number, company_id: number, admin_id: number) {
    let ban_role_time = new Date(Date.now())
    await this.table().update({ ban_role_time, admin_id }).where({ user_id, company_id })
    return true
  }

  async readCompanyRole(user_id: number, company_id: number) {
    let read_time = new Date(Date.now())
    await this.table().update({ read_time }).where({ user_id, company_id })
    return true
  }

  async getCompanyRoleListByAdmin(){
    return await this.table().select("*")
    .select("users.id" ,"users.nickname" , "company_role.company_id" , "company.enable")
    .innerJoin("company", "company_role.company_id","company.id")
    .innerJoin("users","company_role.user_id","users.id")
    .whereNull("users.id")
    .andWhere("company.enable",false)


  }
  async getCompanyRoleListByCompany(company_id: number) {
    return await this.table().select("id", "nickname", "email", "mobile", "icon").innerJoin("users", "user_id", "users.id").where({ company_id })
  }
}