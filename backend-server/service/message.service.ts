import { Knex } from "knex";
import { HttpError } from "../http-error";

export class MessageService {
  constructor(private knex: Knex) {}
  table() {
    return this.knex("message");
  }
  async sendMessage(row: {
    sender_id: number;
    receiver_id?: number;
    content: string;
    job_id?: number;
  }) {
    let send_time = new Date();
    let result: any[] = await this.table()
      .insert({ ...row, send_time })
      .returning("id");
    return result[0].id as number;
  }

  async getUserMessage(id: number , roomNumber : number, item: number) {
    let result = await this.knex.raw(/* sql */ `
    with
  pm_receive_message as (
  select sender_id as user_id, message.id , users.nickname , content , send_time from message
  inner join users on users.id = message.sender_id
  where job_id is null
    and sender_id = :id and receiver_id = :roomNumber
),
pm_send_message as (
  select sender_id as user_id, message.id, users.nickname , content , send_time from message
  inner join users on users.id = message.sender_id
  where job_id is null
    and receiver_id = :id and sender_id = :roomNumber
),
pm_message as (
  select id, user_id, content , send_time from pm_send_message
  union
  select id, user_id, content , send_time from pm_receive_message
)
select * from pm_message order by id DESC LIMIT :item
    `,{id, roomNumber, item})
    return result.rows;
  }

  async getJobMessage(id: number , roomNumber : number, item: number) {
    let result = await this.knex.raw(/* sql */ `
    with
    user_job as (select job.id from job_role 
    inner join job on job.id = job_role.job_id 
    where employee_id = :id and confirm_time notnull and reject_time isnull and job.id = :roomNumber
    )
  select message.id , job_id , users.nickname , users.id  as user_id , content , send_time from message 
  inner join users on users.id = message.sender_id
  inner join job on job.id = message.job_id
  where job_id in (select id from user_job)
  order by message.id DESC LIMIT :item
    `,{id, roomNumber, item})
    return result.rows;
  }

  async getMessageList(id: number) {
    let result = await this.knex.raw(/* sql */`
    with
    user_job as (select job.id from job_role 
    inner join job on job.id = job_role.job_id 
    where employee_id = :id and confirm_time notnull and reject_time isnull 
    ),
    job_message as (
    select max(id) as id
    from message
    where job_id in (select id from user_job)
    group by job_id), 
    pm_receive_message as (
    select receiver_id as other, id from message
    where job_id is null
      and sender_id = :id
  ),
  pm_send_message as (
    select sender_id as other, id
    from message
    where job_id is null
      and receiver_id = :id
  ),
  pm_message as (
    select id, other from pm_send_message
    union
    select id, other from pm_receive_message
  ), 
  matched_message as (
    select max(id) as id from pm_message group by other
    union
    select id from job_message
  )
  select message.id, send.id as sender_id ,send.nickname as sender_name ,send.icon as sender_icon, 
  receiver.id as receiver_id ,receiver.nickname as receiver_name ,receiver.icon as receiver_icon,
  job_id , job.name as job_name,
  content ,send_time from message
  inner join users as send on send.id = message.sender_id
  left join users as receiver on receiver.id = message.receiver_id
  left join job on job.id = job_id 
  where message.id in (select id from matched_message)
  order by message.id DESC
`,{id});
return result.rows
  }
}

export class ReadMessageService {
  constructor(private knex: Knex) {}
  table() {
    return this.knex("read_message");
  }
  async createReadMessage(message_id: number, user_id: number) {
    let row = await this.checkReadMessage(message_id, user_id);
    if (row) {
      return;
    }
    let read_time = new Date(Date.now());
    let result: any[] = await this.table()
      .insert({ message_id, user_id, read_time })
      .returning("id");
    return result[0].id as number;
  }
  async checkReadMessage(message_id: number, user_id: number) {
    let row = await this.table()
      .select()
      .where({ message_id, user_id })
      .first();
    return row;
  }
}
