import { Knex } from "knex";
import { HttpError } from "../http-error";
import { Employee } from "../models";



export class EmployeeService {
  constructor(private knex: Knex) {}
  table() {
    return this.knex("employee");
  }

  async createEmployee( user_id : number, employee: Employee){
    let row = await this.table().select().where({ user_id }).first();
    if (row) {
      return await this.updateEmployee(user_id, employee)
    }
    let rows: any[] = await this.table().insert({ ...employee, user_id }).returning("user_id");
    return {message: "create employee " + rows[0].user_id}
  }

  async updateEmployee( user_id : number, employee: Employee){
    await this.table().update( employee ).where({ user_id })
    return {message: "update employee " + user_id}
  }

  async getAttachLink ( user_id : number){
    return await this.table().select("attach").whereNotNull("attach").andWhere({user_id}).first()
  }

  async getEmployeeProfile (user_id : number){
    return await this.knex('users').select("nickname", "gender", "salary", "bank_account", "fullname","icon", "attach")
    .leftJoin("employee","employee.user_id","users.id")
    .where({id : user_id})
    .first()
  }
  async getEmployeeById(fullname: string) {
    let row = await this.table().select("user_id").where({fullname }).first();
    if (!row) {
      return null;
    }
    return row.user_id as number;
  }

  async getPendingJobList(user_id: number) {
    let knex = this.knex
    let employee = await knex
      .from('employee')
      .where({ user_id })
      .select('salary')
      .first()
    if (!employee) {
      throw new HttpError('employee profile not found', 404)
    }
    let minDailySalary = employee.salary
    knex
      .from('job')
      .whereNotIn(
        'job.id',
        knex.from('job_role').where('employee_id', user_id).select('job_id'),
      )
      .whereIn(
        'job.district_id',
        knex
          .from('employee_district')
          .where('user_id', user_id)
          .select('district_id'),
      )
      // .where(
      //   knex.raw(
      //     /* sql */
      //     `salary / (extract(epoch from (job.end_date - job.start_date)) / 3600 / 12) >= ?`,
      //     [minDailySalary],
      //   ),
      // )
      .where('start_date', '>', knex.fn.now())
      .where(
        'require_worker_num',
        '>',
        knex.from('job_role').whereNotNull('reject_time').count(),
      )
  }

}
