import { Knex } from "knex";
import { HttpError } from "../http-error";

export class AwardService {
  constructor(private knex: Knex) {}
  table() {
    return this.knex("award");
  }
  async createAward(employee_id : number, award_type_id : number , amount: number,desc?: string) {
      let result:any[] = await this.table().insert({employee_id, award_type_id, amount, desc}).returning("id");
      return result[0].id as number;
    }
  async updateAward(id : number, employee_id : number, award_type_id : number , amount: number,desc?: string) {
      let result = await this.table().update({employee_id, award_type_id, amount, desc}).where({id})
      return result
    }

  async unPaymentList() {
    let row = await this.table().select().whereNull("payment_id")
    if (!row) {
      throw new HttpError("award not found", 404);
    }
    return row
  }

  async paymentAward(id : number, payment_id : number){
    let result = await this.table().update({payment_id}).where({id})
    return result
  }


  async getAwardList(){
    return await this.table().select()
  }
}

export class AwardTypeService {
  constructor(private knex: Knex) {}
  table() {
    return this.knex("award_type");
  }
  async createAwardType(name: string){
    let id = this.getAwardTypeByName(name)
    if(id){return id}
    let result:any[] = await this.table().insert({name}).returning("id");
    return result[0].id as number;
  }

  async getAwardTypeByName(name: string){
    let result = await this.table().select("id").where({name}).first()
    if (!result){return null}
    return result.id as number;
}
  async getAwardTypeList(){
    return await this.table().select()
  }

}