import multer from 'multer'
import aws from 'aws-sdk'
import multerS3 from 'multer-s3'
import { env } from './env'
import express from 'express'

export let s3 = new aws.S3({
  accessKeyId: env.aws.accessKeyId,
  secretAccessKey: env.aws.secretAccessKey,
  region: env.aws.s3Region,
})

let fileCounter = 0
let s3Storage = multerS3({
  s3,
  bucket: env.aws.s3Bucket,
  metadata: (req, file, cb) => {
    console.log('s3 object metadata, file:', file)
    if (file.size > 10 * 1024 ** 2) {
      cb(new Error('file so big'))
      return
    }
    cb(null, { fieldName: file.fieldname })
  },
  key: (req, file, cb) => {

    console.log('s3 object key, file:', file)
    let now = Date.now()
    fileCounter++
    let ext = file.mimetype.split('/').pop()!
    ext = ext.split('-').pop()!
    ext = ext.split(';')[0]
    if (file.fieldname == 'attach_file') {
      switch (ext) {
        case 'pdf':
          cb(null, `${file.fieldname}/${now}-${fileCounter}.${ext}`);
          break
        default:
          // (req as express.Request).res.status(400).json({ message: 'Invalid pdf format' })  //error !!!
          cb(new Error('invalid pdf format'))
          break
      }
    } else if (file.fieldname) {

      switch (ext) {
        //TODO: add all mimetype of photo
        case 'jpeg':
        case 'png':
        case 'gif':
          cb(null, `${file.fieldname}/${now}-${fileCounter}.${ext}`);
          break
        default:
          // req.res?.status(400).json({ message: 'Invalid image format' })
          cb(new Error('invalid image format'))
          break
      }
    } else {
      // req.res?.status(400).json({ message: 'Invalid field' })
      cb(new Error('invalid field'))
    }
  },
})

let fileStorage = multer.diskStorage({
  destination: './uploads',
  filename: (req, file, cb) => {
    let now = Date.now()
    // image/jpeg or image/png
    fileCounter++
    let ext = file.mimetype.split('/').pop()!
    ext = ext.split('-').pop()!
    ext = ext.split(';')[0]
    if (file.fieldname == 'attach_file') {
      switch (ext) {
        case 'pdf':
          cb(null, `${file.fieldname}-${now}-${fileCounter}.${ext}`);
          break
        default:
          req.res?.status(400).json({ message: 'Invalid pdf format' })
          cb(new Error('invalid pdf format'), '')
          break
      }
    } else if (file.fieldname) {

      switch (ext) {
        //TODO: add all mimetype of photo
        case 'jpeg':
        case 'png':
        case 'gif':
          cb(null, `${file.fieldname}-${now}-${fileCounter}.${ext}`);
          break
        default:
          req.res?.status(400).json({ message: 'Invalid image format' })
          cb(new Error('invalid image format'), '')
          break
      }
    } else {
      req.res?.status(400).json({ message: 'Invalid field' })
      cb(new Error('invalid field'), '')
    }
  },
})

export let upload = multer({
  storage: env.nodeEnv === 'development' ? fileStorage : s3Storage,
  limits: {
    files: 5,
    fileSize: 10 * 1024 ** 2
  }
})
