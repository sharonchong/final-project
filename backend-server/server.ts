import http from 'http'
import express from 'express'
import { apiRoutes } from './api'
import cors from 'cors'
import {Server as SocketIO} from 'socket.io';
import { setIO } from "./socket-io";
import path from "path"

export type Server = {
  close: () => Promise<void>
}

export function startServer(port: number) {
  let app = express()
  app.use(cors())
  app.use(express.static('public'))
  app.use(express.static('uploads'))


  app.use(express.json())
  // app.use(express.raw({type:'form-data-/multi-part',limit: '50mb'}))
  app.use(express.urlencoded({ extended: false }))

  app.use(apiRoutes)


  let server = http.createServer(app)
  let io = new SocketIO(server, {cors: {origin: '*'}});
  setIO(io)

  app.use((req, res) => {
    console.log("404:", req.method, req.url)
    res.sendFile(path.resolve(path.join("public", "404.html")))
  })

  function close() {
    return new Promise<void>((resolve, reject) => {
      server.close(err => {
        if (err) {
          reject(err)
        } else {
          resolve()
        }
      })
    })
  }
  return new Promise<Server>((resolve, reject) => {
    try {
      server.listen(port, () => {
        resolve({
          close,
        })
      })
    } catch (error) {
      reject(error)
    }
  })
}