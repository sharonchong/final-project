function submit() {
  console.log("submit");
  let startDate = new Date("2022-03-10T15:30").toISOString();
  let endDate = new Date("2022-03-15T13:30").toISOString();
  let startTime = "08:00";
  let endTime = "19:00";

  let now = new Date(startDate);
  let end = new Date(endDate);
  type Slot = {
    start_time: string;
    end_time: string;
  };
  let slots: Slot[] = [];
  for (; now.getTime() < end.getTime(); ) {
    let start_time = now.toISOString();
    let [h, m] = endTime.split(":");
    now.setHours(+h, +m, 0, 0);
    let end_time = now.toISOString();
    slots.push({ start_time, end_time });
    [h, m] = startTime.split(":");
    now.setHours(24 + +h, +m, 0, 0);
  }
  if (slots.length > 0) {
    let last = slots[slots.length - 1];
    let date = new Date(last.end_time);
    let end = new Date(endDate);
    date.setHours(end.getHours(), end.getMinutes(), 0, 0);
    last.end_time = date.toISOString();
  }

  console.log(
    slots.map((slot) => {
      return {
        s: new Date(slot.start_time).toString(),
        e: new Date(slot.end_time).toString(),
      };
    })
  );
}
submit();
setTimeout(submit, 10000000);
