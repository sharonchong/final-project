import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.alterTable("admin", (table) => {
    table.dropColumn("is_admin");
    table.timestamp("delete_time");
  });
  await knex.schema.alterTable("company_role", (table) => {
    table.dropColumn("approve_admin_time");
    table.dropColumn("reject_admin_time");
  });

  await knex.schema.alterTable("job", (table) => {
    table.dropColumn("start_work");
    table.dropColumn("end_work");
    table.dropColumn("attendance");    
  });

  await knex.schema.createTable("job_time", (table) => {
    table.increments("id");
    table.integer("job_id").references("job.id");
    table.timestamp("start_time");
    table.timestamp("end_time");
  });
  await knex.schema.alterTable("users", (table) => {
    table.boolean("enable").defaultTo(false).alter();
    table.dropColumn("verify_code");
    table.dropColumn("verify_expire_time");
  });

  await knex.schema.alterTable("company", (table) => {
    table.boolean("enable").defaultTo(false);
  });

}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable("company", (table) => {
    table.dropColumn("enable");
  });

  await knex.schema.alterTable("users", (table) => {
    table.boolean("enable").defaultTo(true).alter();
    table.timestamp("verify_expire_time");
    table.string("verify_code", 64);
  });
  await knex.schema.dropTableIfExists("job_time");
  await knex.schema.alterTable("job", (table) => {
    table.timestamp("start_work");
    table.timestamp("end_work");
    table.integer("attendance");
  });
  await knex.schema.alterTable("company_role", (table) => {
    table.timestamp("approve_admin_time");
    table.timestamp("reject_admin_time");
  });
  await knex.schema.alterTable("admin", (table) => {
    table.boolean("is_admin").defaultTo(true);
    table.dropColumn("delete_time");
  });

}
