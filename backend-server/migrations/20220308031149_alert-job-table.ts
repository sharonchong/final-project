import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable("job", (table) => {
        table.timestamp('startDate')
        table.timestamp('endDate')
        table.time('dailyStartTime')
        table.time('dailyEndTime')

    });

}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable("job", (table) => {
        table.dropColumn('startDate')
        table.dropColumn('endDate')
        table.dropColumn('dailyStartTime')
        table.dropColumn('dailyEndTime')
    });
}

