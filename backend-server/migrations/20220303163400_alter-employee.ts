import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.alterTable("employee", (table) => {
    table.dropColumn("start_time");
    table.dropColumn("end_time");
    table.dropColumn("district_id");
  });
  await knex.schema.createTable("employee_district", (table) => {
    table.increments("id");
    table.integer("user_id").references('users.id');
    table.integer("district_id").references('district.id');
});
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable('employee_district')
  await knex.schema.alterTable("employee", (table) => {
    table.time("start_time");
    table.time("end_time");
    table.integer("district_id").references('district.id');
  });
}
