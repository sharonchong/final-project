import { IonSelect, IonSelectOption } from "@ionic/react";
import { useState } from "react";
import { uploadHandler } from "./updateUtilis";

function SelectInputComp({ defaultValue, data , userId, attribute, tableName }:any){
    const [ currentValue, setCurrentValue ] = useState<string>(defaultValue)
  
    async function changeHandler(val:string){
      setCurrentValue(val);
      console.log(val, userId, attribute);

      let isSuccess = await uploadHandler(val, userId, attribute, tableName);
      console.log(isSuccess);

      //fetch
    }
  
    return (
      <div>
        <IonSelect value={currentValue} placeholder="Select One" onIonChange={e => changeHandler(e.detail.value)}>
          { data.map( (v:string) => <IonSelectOption key={v} value={v}> {v} </IonSelectOption> ) }
        </IonSelect>
      </div>
    )
}

export default SelectInputComp