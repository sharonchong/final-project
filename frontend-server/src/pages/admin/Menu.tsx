import {
  IonContent,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonMenu,
  IonMenuToggle,
  IonNote,
} from '@ionic/react';

import { useLocation } from 'react-router-dom';
import { constructOutline, homeOutline, reloadOutline, ribbonOutline,  walkOutline } from 'ionicons/icons';
import { routes } from '../../routes';
import './Menu.css';

interface AppPage {
  url: string;
  iosIcon: string;
  mdIcon: string;
  title: string;
}


const Menu: React.FC = () => {
  const location = useLocation();
const appPages: AppPage[] = [
  {
    title: '公司名單',
    url:routes.admin.companyList ,
    iosIcon: homeOutline,
    mdIcon: homeOutline
  },
  {
    title: '員工名單',
    url:routes.admin.userList,
    iosIcon: homeOutline,
    mdIcon: homeOutline
  },
  {
    title: '公司審批',
    url: routes.admin.companyApproval,
    iosIcon: reloadOutline,
    mdIcon: reloadOutline
  },
  {
    title: '員工審批',
    url: routes.admin.employeeApproval,
    iosIcon: reloadOutline,
    mdIcon: reloadOutline,
  },
  {
    title: '工作項目',
    url: routes.admin.projectDetails,
    iosIcon: constructOutline,
    mdIcon: constructOutline,
  },
  {
    title: '出勤/付款管理',
    url: routes.admin.attendancePayment,
    iosIcon: walkOutline,
    mdIcon: walkOutline,
  },
  {
    title: '獎賞系統管理',
    url:routes.admin.rewardSystem,
    iosIcon:ribbonOutline,
    mdIcon: ribbonOutline,
  },
  // {
  //   title: '整體資料',
  //   url: routes.admin.reportStatement,
  //   iosIcon: folderOpenOutline,
  //   mdIcon: folderOpenOutline,
  // },
];
  return (
    <IonMenu contentId="main" type="overlay">
      <IonContent>
        <IonList id="inbox-list">
          <IonListHeader>管理員控制台</IonListHeader>
          <IonNote>alexlau@tecky.io</IonNote>
          {appPages.map((appPage, index) => {
            return (
              <IonMenuToggle key={index} autoHide={false}>
                <IonItem className={location.pathname === appPage.url ? 'selected' : ''} routerLink={appPage.url} routerDirection="none" lines="none" detail={false}>
                  <IonIcon slot="start" ios={appPage.iosIcon} md={appPage.mdIcon} />
                  <IonLabel>{appPage.title}</IonLabel>
                </IonItem>
              </IonMenuToggle>
            );
          })}
        </IonList>
      </IonContent>
    </IonMenu>
  );
};

export default Menu;
