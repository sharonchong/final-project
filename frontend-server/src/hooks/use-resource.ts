import { post, postWithFile } from "../api";
import { useGet } from './use-get'

export function useResource<T extends { error: string }>(
  url: string,
  initial: T,
  storage?: Storage,
) {
  const get = useGet(url, initial, storage)
  const { state, setState } = get
  function upload() {
    return post(url, state)
  }
  function uploadFile() {
    let formData = new FormData()
    let data = state as any
    for (let key in data) {
      let value = data[key]
      formData.append(key, value)
    }
    return postWithFile(url, formData)
  }
  function patchDraftData(patch: Partial<T>) {
    setState(state => ({
      ...state,
      ...patch,
    }))
  }
  return {
    ...get,
    save: upload,
    saveWithFile: uploadFile,
    patchDraftData,
  }
}
