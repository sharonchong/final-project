import { env } from "./env";
import { updateToken } from "./redux/auth/thunk";
import { store } from "./redux/store";

export async function handleFetchResult(resP: Promise<Response>) {
  try {
    let res = await resP;
    let json = await res.json();
    if (json.token) {
      let token = json.token;
      store.dispatch(updateToken(token));
    }
    return json;
  } catch (error) {
    return { error: String(error) };
  }
}

let apiOrigin = env.API_ORIGIN;

export function get(url: string) {
  let token = store.getState().auth.user?.token;
  return handleFetchResult(
    fetch(apiOrigin + url, {
      headers: {
        Authorization: "Bearer " + token,
      },
    })
  );
}

export function post(url: string, body?: object) {
  let token = store.getState().auth.user?.token;
  return handleFetchResult(
    fetch(apiOrigin + url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
      body: JSON.stringify(body),
    })
  );
}
export function postWithFile(url: string, body: FormData) {
  let token = store.getState().auth.user?.token;
  // let body = new FormData()
  // body.append('photo',file)
  return handleFetchResult(
    fetch(apiOrigin + url, {
      method: "POST",
      headers: {
        Authorization: "Bearer " + token,
      },
      body,
    })
  );
}

// export async function getReport() {
//   return get('/report')
// }

// export async function postReport(mood: string) {
//   return post('/report', { mood })
// }
