import { AuthAction } from "./action";
import { AuthState, AuthUser } from "./state";
import jwtDecode from "jwt-decode";
import { storage } from "../../storage";

export type JWTPayload = {
  id: number 
  exp: number
  is_admin: true
  is_employee : false
  nickname: string
  company_id: number
  enable : boolean
}
| {
  id: number  
  is_admin: false
  nickname: string
  is_employee : boolean
  company_id: number
  enable : boolean
}

function loadUserFromToken(token: string | null): AuthUser | null {
  if (!token) {
    return null;
  }
  try {
    let payload: JWTPayload = jwtDecode(token);
    return { id : payload.id, nickname: payload.nickname, token , is_admin: payload.is_admin, company_id: payload.company_id, enable: payload.enable, is_employee : payload.is_employee};
  } catch (error) {
    console.error("Failed to code JWT:", error);
    return null;
  }
}

function initialState(): AuthState {
  let token = storage.token;

  return {
    user: loadUserFromToken(token),
    registerResult: { type: "idle" },
    loginResult: { type: "idle" },
  };
}

export function authReducer(
  state: AuthState = initialState(),
  action: AuthAction
): AuthState {
  switch (action.type) {
    case "@@Auth/logout":
      return {
        ...state,
        user: null,
        loginResult: {
          type: "idle",
        },
      };
    case "@@Auth/setRegisterResult":
      return {
        ...state,
        registerResult: action.result,
        user: action.result.type === "success" ? loadUserFromToken(action.result.token): state.user
      };
    case "@@Auth/setLoginResult":
      return {
        ...state,
        loginResult: action.result,
        user: loadUserFromToken(
          action.result.type === 'success' ? action.result.token : null
        ),
        
      };
    default:
      return state;
  }
}
